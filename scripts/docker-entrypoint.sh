#!/bin/bash

while true; do
    if psql -tAc "SELECT 1";  then
        break
    else
        echo "Waiting for the database connection"
        sleep 1;
    fi
done

set -e

/tigase/scripts/init-db.sh

cp etc/init.properties.template etc/init.properties

echo "--virt-hosts=${TIGASE_DOMAIN}:-register" >> etc/init.properties
echo "--admins=admin@${TIGASE_DOMAIN}" >> etc/init.properties
echo "--user-db-uri = jdbc:postgresql://${PGHOST}:${PGPORT}/${PGDATABASE}?user=${PGUSER}&password=${PGPASSWORD}" >>etc/init.properties

chown -R tigase:tigase /tigase
chmod -R a+x /tigase/scripts

su -m tigase -c "/tigase/scripts/tigase.sh run /tigase/etc/tigase.conf"



