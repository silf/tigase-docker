#!/bin/bash

export EXPECTED_SCHEMA_VER=5.1
export SCHEMA_VER=$(psql -tAc "select tiggetdbproperty('schema-version')")

# psql script above may fail so set -e after it
set -e

function updatePasswordFile(){
    sed s/"jidDomain"/"${TIGASE_DOMAIN}"/ $1 | sed -e s/"ADMIN_PASSWORD"/"${ADMIN_PASSWORD}"/ | sed -e s/"ATHENA_TIGASE_PASSWORD"/"${ATHENA_TIGASE_PASSWORD}"/
}

if test $SCHEMA_VER = $EXPECTED_SCHEMA_VER ;
then
    updatePasswordFile database/docker-update-basic-users.sql | psql
    echo "No need to update schema"
    exit 0
fi

psql -f database/postgresql-schema-5-1.sql
updatePasswordFile database/docker-add-basic-users.sql | psql
