# Docker container for Tigase 5.2 server

Dockerfile

1. Sets-up all variables
2. Downloads Tigase and unpacks it
3. Copies contents of the `override` directory, so one can override contents
   of downloaded tigase server. Right now we use slightly patched version of tigase
   (see: https://bitbucket.org/silf/tigase-server).

Start command:

1. Set's up postgresql connection
2. Installs schema in the database if schema is not present.
3. Creates user admin and ensures it has password equal to `ADMIN_PASSWORD`

## Variables

* `TIGASE_DOMAIN` --- domain used by this docker instance, and JID domain
* `PGHOST` database host
* `PGPORT` database port
* `PGDATABASE` database
* `PGUSER` database user
* `PGPASSWORD` database password
* `TIGASE_SERVER_SOURCE` url from which to download tigase server
* `ADMIN_PASSWORD` password for admin user in tigase
* `ATHENA_TIGASE_PASSWORD` password for athena user in tigase

When re-running this container it will update admin password,
but it will not touch `jidDomain` so if it changed it will not be updated,
and probably things will break.

# How to update this to another version

1. Update `TIGASE_SERVER_SOURCE`
2. Probably you'll need to update postgresql scripts
3. You'll need to regenerate overrides
